package com.example.criminalintent.model;

import java.util.Date;
import java.util.UUID;

public class Crime
{
    private UUID mId;
    private String mTitle;
    private Date mDate;
    private boolean mSolved;

    public Crime()
    {
        mId = UUID.randomUUID();
        mDate = new Date();
    }

    public UUID getId()
    {
        return mId;
    }

    public String getTitle()
    {
        return mTitle;
    }

    public Crime setTitle(String title)
    {
        mTitle = title;
        return this;
    }

    public Date getDate()
    {
        return mDate;
    }

    public Crime setDate(Date date)
    {
        mDate = date;
        return this;
    }

    public boolean isSolved()
    {
        return mSolved;
    }

    public Crime setSolved(boolean solved)
    {
        mSolved = solved;
        return this;
    }
}
